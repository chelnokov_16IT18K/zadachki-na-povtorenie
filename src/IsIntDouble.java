import java.util.Scanner;

public class IsIntDouble {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("Введите вещественное число");
        double doubleNumber = reader.nextDouble();
        int intNumber = (int)doubleNumber;
        double result = doubleNumber - intNumber;
        if(result == 0){
            System.out.println("ДА, число действительно целое");
        }else {
            System.out.println("НЕТ, ни капельки оно не целое");
        }
    }
}
